package com.intellicar.finder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;


import com.intellicar.finder.alert_dialogs.LoadingDialog;
import com.intellicar.finder.utils.GlobalPreferManager;

import java.util.ArrayList;


public class BaseActivity extends AppCompatActivity {

    ArrayList<String> permissions = new ArrayList<>();

    LoadingDialog progressDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public void showProgress(String message) {
//        progressDialogFragment = LoadingDialog.newInstance(message);
//        progressDialogFragment.show(getFragmentManager(), "progress_dialog");
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new LoadingDialog().newInstance(message), "LoadingDialog")
                .addToBackStack("FRAGMENT")
                .commit();
        GlobalPreferManager.setBoolean("IS_LOADING",true);
    }

    public void dismissProgress() {
        GlobalPreferManager.setBoolean("IS_LOADING",false);
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                // Open the navigation drawer when the home icon is selected from the toolbar.
                super.onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


}
