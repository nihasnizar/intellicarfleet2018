package com.intellicar.finder.ui.home_view;



import com.intellicar.finder.ICommonControllerCallback;

import org.json.JSONObject;

/**
 * Created by VIDHU on 4/16/2017.
 */

public interface IHomeViewControllerCallback extends ICommonControllerCallback {


    void getVehiclesSuccess(JSONObject responseObj);

    void getVehiclesFailed(String message);
}
