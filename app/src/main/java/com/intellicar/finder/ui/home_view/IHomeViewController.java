package com.intellicar.finder.ui.home_view;


import com.intellicar.finder.ICommonController;

/**
 * Created by VIDHU on 4/16/2017.
 */

public interface IHomeViewController extends ICommonController {

    void getVehicles(String pgrouppath, String token);
}
