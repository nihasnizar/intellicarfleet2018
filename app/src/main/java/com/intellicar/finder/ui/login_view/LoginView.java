package com.intellicar.finder.ui.login_view;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.intellicar.finder.R;
import com.intellicar.finder.activities.MainActivity;
import com.intellicar.finder.utils.GlobalPreferManager;
import com.intellicar.finder.OnConfirmAlertDialogClickListner;
import com.intellicar.finder.utils.anims.Fade;
import com.intellicar.finder.utils.anims.TransitionManager;
import com.intellicar.finder.utils.anims.TransitionSet;
import com.intellicar.finder.utils.extra.Scale;
import com.intellicar.finder.utils.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by snyxius on 3/1/17.
 */


public class LoginView extends Fragment implements ILoginViewControllerCallback,OnConfirmAlertDialogClickListner{

    //BUTTERKNIFE
    @BindView(R.id.the_progress_bar)ProgressBar progressBar;
    @BindView(R.id.the_login_progress)RelativeLayout loginProgress;
    @BindView(R.id.login_layout)LinearLayout loginLayout;
    @BindView(R.id.the_button_login)Button loginButton;
    @BindView(R.id.username_layout)TextInputLayout username_layout;
    @BindView(R.id.username)EditText username_edt;
    @BindView(R.id.password_layout)TextInputLayout password_layout;
    @BindView(R.id.password)EditText password_edt;
    @BindView(R.id.animate_logo_layout)LinearLayout animateLogoLayout;

    //OTHERS
    private ILoginViewController iLoginViewController;
    OnConfirmAlertDialogClickListner onConfirmAlertDialogClickListner;

    public static LoginView newInstance() {
        Bundle args = new Bundle();
//        position = flag;
        LoginView fragment = new LoginView();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        exitFullscreen(getActivity());
        View view = inflater.inflate(R.layout.login_view, container, false);
        ButterKnife.bind(this,view);

//
        return view;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        onConfirmAlertDialogClickListner=this;
        iLoginViewController = new LoginViewController(this);
        loginLayout.setVisibility(View.INVISIBLE);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, -1.2f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        animateLogoLayout.startAnimation(slide);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                boolean visible=true;
                TransitionSet set = new TransitionSet()
                        .addTransition(new Scale(0.7f))
                        .addTransition(new Fade())
                        .setInterpolator(visible ? new LinearOutSlowInInterpolator() : new FastOutLinearInInterpolator());
                TransitionManager.beginDelayedTransition(loginLayout, set);
                loginLayout.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
            }
        },200);
    }



    public void exitFullscreen(Activity activity) {
        if (Build.VERSION.SDK_INT > 10) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            activity.getWindow()
                    .setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
    }



    private boolean validateField(EditText edt, TextInputLayout txi) {
        if (edt.getText().toString().trim().isEmpty()) {
            txi.setErrorEnabled(true);
            txi.setError("Field Required");
            edt.requestFocus();
            return false;
        } else {
            txi.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateField(EditText edt) {
        if (edt.getText().toString().trim().isEmpty()) {
            edt.setError("Field Required");
            edt.requestFocus();
            return false;
        }

        return true;
    }


    //ONCLICKS
    @OnClick(R.id.the_button_login)
    public void loginButtonClick(){
        if(validateField(username_edt,username_layout)) {
            if (validateField(password_edt, password_layout)) {
                loginProgress.setVisibility(View.VISIBLE);
                iLoginViewController.login(username_edt.getText().toString().trim(), password_edt.getText().toString().trim());
            }
        }


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startActivity(new Intent(getActivity(), HomeActivity.class));
//                getActivity().finish();
//            }
//        },2000);
    }


    @Override
    public void showProgressIndicator(boolean show) {

    }

    @Override
    public void showServerErrorMessage(String message) {
//        CommonUtils.showConfirmAlertFragmentDialogString(getActivity(),getParentFragment(),"Error",message,"OK","CANCEL",R.drawable.warning,onConfirmAlertDialogClickListner);
    }




    @Override
    public void onLoginSuccess(JSONObject responseObj) {
        loginProgress.setVisibility(View.GONE);
        try {
            String userId=responseObj.getJSONObject("data").getJSONObject("userinfo").getString("userid");
            String token=responseObj.getJSONObject("data").getString("token");
            GlobalPreferManager.setString("TOKEN",token);
            GlobalPreferManager.setString("USERID",userId);
            CommonUtils.showToast(getActivity(),"Login Success");
            GlobalPreferManager.setBoolean("IS_LOGIN_DONE", true);

            startActivity(new Intent(getActivity(), MainActivity.class));
            GlobalPreferManager.setBoolean("IN_SPLASH_SCREEN", false);
            getActivity().finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoginFailed(String message) {
        loginProgress.setVisibility(View.GONE);
//        CommonUtils.showConfirmAlertFragmentDialogString(getActivity(),getParentFragment(),"Error",message,"OK","CANCEL",R.drawable.warning,onConfirmAlertDialogClickListner);
    }

    @Override
    public void onPositiveButtonClick(String message) {

    }

    @Override
    public void onNegativeButtonClick() {

    }
}


