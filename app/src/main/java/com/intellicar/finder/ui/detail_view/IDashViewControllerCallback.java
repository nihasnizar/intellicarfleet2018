package com.intellicar.finder.ui.detail_view;



import com.intellicar.finder.ICommonControllerCallback;

import org.json.JSONObject;

/**
 * Created by VIDHU on 4/16/2017.
 */

public interface IDashViewControllerCallback extends ICommonControllerCallback {


    void getVehiclesSuccess(JSONObject responseObj);

    void getVehiclesFailed(String message);
}
