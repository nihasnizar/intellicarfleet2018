package com.intellicar.finder.ui.login_view;


import com.intellicar.finder.ICommonController;

/**
 * Created by VIDHU on 4/16/2017.
 */

public interface ILoginViewController extends ICommonController {

    void login(String username, String password);
}
