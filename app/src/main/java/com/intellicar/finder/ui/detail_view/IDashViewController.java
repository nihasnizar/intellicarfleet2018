package com.intellicar.finder.ui.detail_view;


import com.intellicar.finder.ICommonController;

/**
 * Created by VIDHU on 4/16/2017.
 */

public interface IDashViewController extends ICommonController {

    void getVehicles(String pgrouppath, String token);
}
