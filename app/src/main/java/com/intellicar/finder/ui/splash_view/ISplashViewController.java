package com.intellicar.finder.ui.splash_view;

import com.intellicar.finder.ICommonController;

/**
 * Created by VIDHU on 4/16/2017.
 */

public interface ISplashViewController extends ICommonController {


    void checkToken(String token);
}
