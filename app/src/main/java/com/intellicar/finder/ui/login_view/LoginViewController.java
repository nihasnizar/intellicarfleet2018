package com.intellicar.finder.ui.login_view;

import android.util.Log;

import com.google.gson.JsonObject;
import com.intellicar.finder.apis.Constants;
import com.intellicar.finder.apis.Parse;
import com.intellicar.finder.apis.WebRequests;
import com.intellicar.finder.app.App;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VIDHU on 4/16/2017.
 */

public class LoginViewController implements ILoginViewController {

    private ILoginViewControllerCallback mCallBack;

    public LoginViewController(ILoginViewControllerCallback mCallBack) {
        this.mCallBack = mCallBack;
    }


    @Override
    public void login(String username, String password) {

        mCallBack.showProgressIndicator(true);
        WebRequests apiService =
                App.getApiClient().create(WebRequests.class);


        apiService.login(Parse.loginData(username,password)).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    mCallBack.showProgressIndicator(false);
                    try {
                        JSONObject obj = new JSONObject(response.body().toString());
                        mCallBack.onLoginSuccess(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        JSONObject obj = new JSONObject(response.errorBody().string());
                        mCallBack.onLoginFailed(obj.getJSONObject("err").getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("FALURE >> ", call.toString());
                mCallBack.showProgressIndicator(false);
                mCallBack.onLoginFailed(Constants.MESSAGE_SERVER_ERROR);
            }
        });
    }

//    private ArrayList<MyContacts> getUsers(JsonArray users) {
//        Gson gson = new Gson();
//        Type listType = new TypeToken<ArrayList<MyContacts>>() {
//        }.getType();
//
//        return gson.fromJson(users, listType);
//    }
}
