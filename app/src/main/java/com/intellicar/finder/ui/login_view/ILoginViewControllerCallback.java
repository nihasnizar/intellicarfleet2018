package com.intellicar.finder.ui.login_view;



import com.intellicar.finder.ICommonControllerCallback;

import org.json.JSONObject;

/**
 * Created by VIDHU on 4/16/2017.
 */

public interface ILoginViewControllerCallback extends ICommonControllerCallback {


    void onLoginSuccess(JSONObject responseObj);

    void onLoginFailed(String message);
}
