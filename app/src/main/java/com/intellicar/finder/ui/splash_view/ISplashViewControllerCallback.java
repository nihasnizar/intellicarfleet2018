package com.intellicar.finder.ui.splash_view;



import com.intellicar.finder.ICommonControllerCallback;

import org.json.JSONObject;

/**
 * Created by VIDHU on 4/16/2017.
 */

public interface ISplashViewControllerCallback extends ICommonControllerCallback {

    void onTokenSuccess(JSONObject responseObj);
    void onTokenFailed(String message);
}
