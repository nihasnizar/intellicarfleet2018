package com.intellicar.finder.ui.detail_view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.MarkerManager;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.intellicar.finder.OnConfirmAlertDialogClickListner;
import com.intellicar.finder.R;
import com.intellicar.finder.activities.MainActivity;
import com.intellicar.finder.apis.Constants;
import com.intellicar.finder.apis.Parse;
import com.intellicar.finder.apis.WebRequest;
import com.intellicar.finder.app.App;
import com.intellicar.finder.libs.Speedometer;
import com.intellicar.finder.pojo.MyItem;
import com.intellicar.finder.socket.SocketSingleton;
import com.intellicar.finder.ui.home_view.HomeViewController;
import com.intellicar.finder.ui.home_view.IHomeViewController;
import com.intellicar.finder.ui.home_view.IHomeViewControllerCallback;
import com.intellicar.finder.utils.CommonUtils;
import com.intellicar.finder.utils.GlobalPreferManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by snyxius on 3/1/17.
 */


public class DashView extends Fragment implements IHomeViewControllerCallback,OnConfirmAlertDialogClickListner, OnMapReadyCallback,
        ClusterManager.OnClusterClickListener<MyItem>,ClusterManager.OnClusterItemInfoWindowClickListener<MyItem>,
ClusterManager.OnClusterItemClickListener<MyItem>{

    //BUTTERKNIFE
    @BindView(R.id.progress_layout)RelativeLayout progressLayout;
    @BindView(R.id.loading_layout)RelativeLayout loadingLayout;
    @BindView(R.id.percentage)TextView percentage;
    @BindView(R.id.custom_speedometer)Speedometer speedometerCustom;

    //OTHERS
    String CANODO="0";
    String IMMOBILISE_VALUE="0";
    int AC=1,PARKINGBRAKE=2,ENGINE=3,INDICATOR=4,SEATBELT=5,HEADLIGHT=6,MOBILIZE=7,LOCK=8,DOOR_OPEN=9;
    String VP="";
    String VehPref="";
    Socket socket;
    GoogleMap gmap;
    float oldZoomValue=15;
    LatLng latLng=new LatLng(12.9241959,77.6181229);
    Marker marker;
    MarkerManager markerManager;
    private ClusterManager<MyItem> mClusterManager;
    boolean isRed=false,isGreen=false,isBlue=false;
    int imm=0,ignOff=0,ignOn=0;
    ArrayList<MyItem> myItemArrayList=new ArrayList<>();
    JSONArray finalAllVehicleArray=new JSONArray();
    boolean isFirstTimeCluster=false;
    Bitmap icon_red,icon_green,icon_blue;
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466));

    private IHomeViewController iDashViewController;
    OnConfirmAlertDialogClickListner onConfirmAlertDialogClickListner;

    public static DashView newInstance() {
        Bundle args = new Bundle();
//        position = flag;
        DashView fragment = new DashView();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dash_view, container, false);
        ButterKnife.bind(this,view);

//
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment mapFragment =
                (SupportMapFragment)
                        getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        icon_blue= CommonUtils.getBitmapMarker(R.drawable.blue_dot_01,70,70);
        icon_red= CommonUtils.getBitmapMarker(R.drawable.red_dot_01,70,70);
        icon_green= CommonUtils.getBitmapMarker(R.drawable.green_dot_01,70,70);

        onConfirmAlertDialogClickListner=this;
        iDashViewController = new HomeViewController(this);

//        connectSocket();
    }

    public void showProgressUpdate(int progress){
            if(progress>=0) {
                progressLayout.setVisibility(View.VISIBLE);
                percentage.setText(progress + "");
            }else{
                progressLayout.setVisibility(View.GONE);
            }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap=googleMap;
        gmap.getUiSettings().setZoomControlsEnabled(true);
    }

    public void disableLiveTrack() {
        speedometerCustom.setVisibility(View.GONE);
        mClusterManager.clearItems();
        disconnectSocket();
        if(marker!=null) {
            marker.remove();
        }
        if(!GlobalPreferManager.getString("ALL_CARS_ARRAY", "").equals("")){
            HandleAllMarkerBG();
        }
    }

    @Override
    public boolean onClusterItemClick(MyItem myItem) {
        disconnectSocket();
        ((MainActivity)getActivity()).startSingleTrack();
        gmap.clear();
        mClusterManager.clearItems();
        if(marker!=null) {
            marker.remove();
        }
        VP=myItem.getVp();
        VehPref=myItem.getVehPrefData();
        connectSocket();
        speedometerCustom.setVisibility(View.VISIBLE);
        speedometerCustom.setRPM(0);
        speedometerCustom.setFuelLevel(0);
        speedometerCustom.setTemperature(0);
        speedometerCustom.updateSpeed("0");
        speedometerCustom.updateodometer("000000");
        return true;
    }

    public void HandleAllMarkerBG(){
        new HandleAllMarkers().execute();
    }

    @Override
    public boolean onClusterClick(Cluster<MyItem> cluster) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }

        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            gmap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void onClusterItemInfoWindowClick(MyItem myItem) {

    }


    @Override
    public void showProgressIndicator(boolean show) {

    }

    @Override
    public void showServerErrorMessage(String message) {
//        CommonUtils.showConfirmAlertFragmentDialogString(getActivity(),getParentFragment(),"Error",message,"OK","CANCEL",R.drawable.warning,onConfirmAlertDialogClickListner);
    }




    @Override
    public void getVehiclesSuccess(JSONObject responseObj) {
        loadingLayout.setVisibility(View.GONE);
    }

    @Override
    public void getVehiclesFailed(String message) {

//        CommonUtils.showConfirmAlertFragmentDialogString(getActivity(),getParentFragment(),"Error",message,"OK","CANCEL",R.drawable.warning,onConfirmAlertDialogClickListner);
    }

    @Override
    public void onPositiveButtonClick(String message) {

    }

    @Override
    public void onNegativeButtonClick() {

    }




    class OwnIconRenderedInner extends DefaultClusterRenderer<MyItem> {

        private final IconGenerator mIconGenerator = new IconGenerator(getActivity());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getActivity());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;


        public OwnIconRenderedInner(Context context, GoogleMap map,
                                    ClusterManager<MyItem> clusterManager) {
            super(context, map, clusterManager);

            View multiProfile = getActivity().getLayoutInflater().inflate(R.layout.custom_cluster, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getActivity());
            mDimension = 50;
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = 2;
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(MyItem item, MarkerOptions markerOptions) {
            mImageView.setBackgroundColor(Color.parseColor("#5f5e5e"));
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(item.getIcon());
            markerOptions.snippet(item.getSnippet());
            markerOptions.title(item.getTitle());
            markerOptions.rotation(Float.valueOf(item.getRotation()));
            super.onBeforeClusterItemRendered(item, markerOptions);
        }


        @Override
        protected void onBeforeClusterRendered(Cluster<MyItem> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

//            for (MyItem p : cluster.getItems()) {
//                // Draw 4 at most.
//                if (profilePhotos.size() == 4) break;
//                Drawable drawable = getResources().getDrawable(R.drawable.download);
//                drawable.setBounds(0, 0, width, height);
//                profilePhotos.add(drawable);
//            }
//            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
//            multiDrawable.setBounds(0, 0, width, height);

            if(isRed)
                mClusterImageView.setBackgroundColor(Color.parseColor("#ff4955"));
            else if(isGreen)
                mClusterImageView.setBackgroundColor(Color.parseColor("#2ad230"));
            else if(isBlue)
                mClusterImageView.setBackgroundColor(Color.parseColor("#4673ff"));
            else
                mClusterImageView.setBackgroundColor(Color.parseColor("#5f5e5e"));
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

//        @Override
//        protected boolean shouldRenderAsCluster(Cluster cluster) {
//            // Always render clusters.
//            return cluster.getSize() > 1;
//        }

    }




    public class HandleAllMarkers extends AsyncTask<String, Double, JSONObject> {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myItemArrayList=new ArrayList<>();
            ignOff=0;
            ignOn=0;
            imm=0;
            CommonUtils.consoleLog("STARTED HANDLING");

        }

        @Override
        protected JSONObject doInBackground(String... params) {


            try {
                if (mClusterManager != null)
                    mClusterManager.clearItems();

                CommonUtils.consoleLog("DO IN BACKGROUND HANDLING");

                finalAllVehicleArray = new JSONArray(GlobalPreferManager.getString("ALL_CARS_ARRAY", ""));
                for (int k = 0; k < finalAllVehicleArray.length(); k++) {
                    JSONObject innerObj = finalAllVehicleArray.getJSONObject(k);
//                    CarFinderApp.consoleLog(innerObj.toString());
                    int height = 90;
                    int width = 90;
//                    BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.car_marker);
//                    Bitmap b = bitmapdraw.getBitmap();
//                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                    if (innerObj.has("lat")) {
                        final JSONObject vehicleprefdata=new JSONObject(innerObj.getString("vehicleprefdata"));


                        MyItem item = new MyItem(Double.parseDouble(innerObj.getString("lat")), Double.parseDouble(innerObj.getString("lng")),
                                innerObj.getString("vehicleno"),vehicleprefdata.has("model") ? vehicleprefdata.getString("model") : "");
                        item.setVehPrefData(vehicleprefdata.toString());
                        item.setModel(vehicleprefdata.has("model") ? vehicleprefdata.getString("model") : "");
                        item.setVp(innerObj.has("vehiclepath") ? innerObj.getString("vehiclepath") : "");
                        item.setRotation(innerObj.has("dir") ? innerObj.getString("dir") : "");



                        if(innerObj.has("imm")) {
                            if(Integer.parseInt(innerObj.getString("imm"))>0) {
                                item.setIcon(BitmapDescriptorFactory.fromBitmap(icon_red));
                                item.setStatus("RED");
                                imm++;
                            }else {
                                if (innerObj.has("ign")) {
                                    if (innerObj.getString("ign").equals("0")) {
                                        item.setIcon(BitmapDescriptorFactory.fromBitmap(icon_blue));
                                        item.setStatus("BLUE");
                                        ignOff++;
                                    } else {
                                        item.setIcon(BitmapDescriptorFactory.fromBitmap(icon_green));
                                        item.setStatus("GREEN");
                                        ignOn++;
                                    }
                                } else {
                                    item.setIcon(BitmapDescriptorFactory.fromBitmap(icon_red));
                                    item.setStatus("NO_IGN");
                                }
                            }
                        }else{
                            item.setIcon(BitmapDescriptorFactory.fromBitmap(icon_red));
                            item.setStatus("NO_IMM");
                        }
                        if(isRed) {
                            if(item.getStatus().equals("RED"))
                                mClusterManager.addItem(item);
                        }
                        if (isGreen){
                            if(item.getStatus().equals("GREEN"))
                                mClusterManager.addItem(item);
                        }
                        if(isBlue){
                            if(item.getStatus().equals("BLUE"))
                                mClusterManager.addItem(item);
                        }

                        if (!isRed && !isGreen && !isBlue){
                            mClusterManager.addItem(item);
                        }

                        myItemArrayList.add(item);


//                                gmap.addMarker(new MarkerOptions()
//                                        .position(new LatLng(Double.parseDouble(innerObj.getString("lat")), Double.parseDouble(innerObj.getString("lng"))))
//                                        .title(innerObj.getString("vehicleno"))
//                                        .anchor(0.5f, 0.5f)
//                                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

                        builder.include(new LatLng(Double.parseDouble(innerObj.getString("lat")), Double.parseDouble(innerObj.getString("lng"))));


                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return new JSONObject();

        }

        @Override
        protected void onPostExecute(final JSONObject contactsArray) {
            super.onPostExecute(contactsArray);

            CommonUtils.consoleLog("DONE");
            loadingLayout.setVisibility(View.GONE);
//            botomLayout.setVisibility(View.VISIBLE);

//            for(int i=0;i<myItemArrayList.size();i++) {
//                markerManager.newCollection().addMarker(new MarkerOptions()
//                        .position(myItemArrayList.get(i).getPosition())
//                        .title(myItemArrayList.get(i).getTitle())
//                        .anchor(0.5f, 0.5f)
//                        .snippet(myItemArrayList.get(i).getModel())
//                        .icon(myItemArrayList.get(i).getIcon()));
//            }




            LatLngBounds bounds=BOUNDS_INDIA;
//            immCount.setText(imm+"");
//            ignOffCount.setText(ignOff+"");
//            ignOnCount.setText(ignOn+"");
            if(builder!=null) {
                bounds = builder.build();
                CameraUpdate center =
                        CameraUpdateFactory.newLatLngBounds(bounds, 100);//12.9716, 77.5946));
//            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
                if(!isFirstTimeCluster) {
                    if(gmap!=null)
                        gmap.animateCamera(center);
                    isFirstTimeCluster=true;
                }
//            gmap.animateCamera(zoom);
                mClusterManager.cluster();
            }


        }


    }



     /*
    GET VEHICLE API CALL
     */


    public class APIcallGetVehicles extends AsyncTask<String, Double, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try{
                JSONObject obj = WebRequest.postData(params[0], Constants.BASE_URL+Constants.getmyvehicles);
                return obj;
            }catch (Exception e){
                e.printStackTrace();
                return new JSONObject();
            }
        }

        @Override
        protected void onPostExecute(final JSONObject allVehicles) {
            super.onPostExecute(allVehicles);
            loadingLayout.setVisibility(View.GONE);
            try {
                //STORE ALL VEHICLES ARRAY
                GlobalPreferManager.setString("ALL_CARS",allVehicles.getJSONObject("data").getJSONArray("assets").toString());
//                new AddToDb().execute(contactsArray.getJSONObject("data").getJSONArray("assets").toString());
//                connectSocket();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }










    public void connectSocket(){
        socket= SocketSingleton.get(getActivity()).getServerSocket();
        if(!socket.connected()) {
            CommonUtils.consoleLog("CONNECT");
            socket.connect();
            socket.emit("authtoken", GlobalPreferManager.getString("TOKEN", ""));
            socket.on("authsuccess", handleIncomingMessages);
            socket.on("gpsrt", handleGPSRT);
            socket.on("canrt", handleCANRT);
            socket.on("dtcrt", handleDTCRT);
            socket.on("subscribesuccess", handleSubscribeSuccess);
        }
    }

    public void disconnectSocket(){
        if(socket!=null) {
            //CAN DATA
            JSONObject obj2=new JSONObject();

            JSONArray dataArr2=new JSONArray();
            dataArr2.put("can");//can
            try {
                obj2.accumulate("datarequested",dataArr2);

                JSONArray vehiclePathArr2=new JSONArray();
                vehiclePathArr2.put(VP);// instead all pass vehicle path of selected vehicle
                obj2.accumulate("vehiclepath",vehiclePathArr2);

                JSONArray dataArray2=new JSONArray();
                dataArray2.put(obj2);

                JSONObject mainObj2=new JSONObject();
                mainObj2.accumulate("data",dataArray2);

//                CustomerApp.consoleLog("CAN: "+mainObj2.toString());
                socket.emit("unsubscribe",mainObj2);



                socket.off("authsuccess", handleIncomingMessages);
                socket.off("gpsrt", handleGPSRT);
                socket.off("subscribesuccess", handleSubscribeSuccess);
//            socket = null;
                socket.off("canrt", handleCANRT);
                socket.off("dtcrt", handleDTCRT);

                socket.disconnect();
                socket.close();
                CommonUtils.consoleLog("DISCONNECT");


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private Emitter.Listener handleSubscribeSuccess = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    socket.emit("getnextbucket", "");
                }
            });
        }

    };


    private Emitter.Listener handleIncomingMessages = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
//                    CustomerApp.consoleLog("AUTH: "+data.toString());
                    try {
                        JSONObject obj=new JSONObject();

                        JSONArray dataArr=new JSONArray();
                        dataArr.put("gps");//can
                        obj.accumulate("datarequested",dataArr);

                        JSONArray vehiclePathArr=new JSONArray();
                        vehiclePathArr.put("all");// instead all pass vehicle path of selected vehicle
                        obj.accumulate("vehiclepath",vehiclePathArr);

                        JSONArray dataArray=new JSONArray();
                        dataArray.put(obj);

                        JSONObject mainObj=new JSONObject();
                        mainObj.accumulate("data",dataArray);

//                        CustomerApp.consoleLog(mainObj.toString());
                        socket.emit("subscribe",mainObj);
//                            socket.emit("getnextbucket","");

                        //CAN DATA
                        JSONObject obj2=new JSONObject();

                        JSONArray dataArr2=new JSONArray();
                        dataArr2.put("can");//can
                        obj2.accumulate("datarequested",dataArr2);

                        JSONArray vehiclePathArr2=new JSONArray();
                        vehiclePathArr2.put(VP);// instead all pass vehicle path of selected vehicle
                        obj2.accumulate("vehiclepath",vehiclePathArr2);

                        JSONArray dataArray2=new JSONArray();
                        dataArray2.put(obj2);

                        JSONObject mainObj2=new JSONObject();
                        mainObj2.accumulate("data",dataArray2);

//                        CustomerApp.consoleLog("CAN: "+mainObj2.toString());
                        socket.emit("subscribe",mainObj2);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });
        }

    };

    private Emitter.Listener handleGPSRT = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mClusterManager.clearItems();
                        JSONArray data = (JSONArray) args[0];
//                        CustomerApp.consoleLog("GPS: "+data);

//                        if(!dataArrayIndex.contains(data.getInt(0))){
//                            for(int i=0;i<data.getJSONArray(1).length();i++){
//                                JSONObject obj=data.getJSONArray(1).getJSONObject(i);
//                                allLatLngArray.put(obj);
//                            }



                        socket.emit("getnextbucket","");
//                            dataArrayIndex.add(data.getInt(0));
                        for(int i=0;i<data.getJSONArray(1).length();i++) {
                            JSONObject obj = data.getJSONArray(1).getJSONObject(i);

                            if(obj.has("cb"))
                                speedometerCustom.setCarBattery(obj.getString("cb")+"V");
                            if(obj.has("db"))
                                speedometerCustom.setDeviceBattery(obj.getString("db")+"V");

                            speedometerCustom.updateSpeed(obj.getString("sp").equals("NA") ? "0" :obj.getString("sp"));

                            //NEED TO REMOVE AADED 3/8/17
                            if(obj.has("rp"))
                                speedometerCustom.setRPM((int) Double.parseDouble(obj.getString("rp")));
                            if(obj.has("fl"))
                                speedometerCustom.setFuelLevel((int) Double.parseDouble(obj.getString("fl")));
                            if(obj.has("et"))
                                speedometerCustom.setTemperature((int) Double.parseDouble(obj.getString("et")));

                            //GPS TIME
                            if(obj.has("uts")) {
                                speedometerCustom.setGPSTime("GPS: "+returnDateFormatted(Long.parseLong(obj.getString("uts"))));
                            }

                            //IMMOBILISE DATA
                            if(obj.has("im"))
                                IMMOBILISE_VALUE=obj.getString("im");


                            JSONObject vehPref = new JSONObject(VehPref);
                            if(vehPref.has("odofrom")) {
                                if (vehPref.getString("odofrom").equals("gps_odometer")) {
                                    JSONArray arr = vehPref.getJSONArray("odocalibration").getJSONArray(0);
                                    String newOdo = arr.getDouble(2) + obj.getDouble("od") + "";
                                    if (newOdo.contains("."))
                                        speedometerCustom.updateodometer(newOdo.substring(0, newOdo.indexOf(".")));
                                    else
                                        speedometerCustom.updateodometer(newOdo);
                                }else{
                                    if(CANODO.equals("0")){
                                        if (obj.getString("od").contains("."))
                                            speedometerCustom.updateodometer(obj.getString("od").substring(0, obj.getString("od").indexOf(".")));
                                        else
                                            speedometerCustom.updateodometer(obj.getString("od"));
                                    }
//                                    CarFinderApp.consoleLog("GPS_ODO " + obj.getString("od"));
                                }
                            }else{
//                                CarFinderApp.consoleLog("No ODO FROM " + obj.getString("od"));
                                if (obj.getString("od").contains("."))
                                    speedometerCustom.updateodometer(obj.getString("od").substring(0, obj.getString("od").indexOf(".")));
                                else
                                    speedometerCustom.updateodometer(obj.getString("od"));
                            }

                            //LOCATION
//                                allLatLngArray.put(obj);
//                                CarFinderApp.consoleLog(obj.getString("vp"));
                            if (obj.getString("vp").equals(VP)) {
//                                CustomerApp.consoleLog(obj.getString("lt") + " " + obj.getString("ln"));
                                latLng = new LatLng(Double.parseDouble(obj.getString("lt")), Double.parseDouble(obj.getString("ln")));

//                                if(dateText.getText().toString().equalsIgnoreCase("Live Tracking")) {
                                if(marker==null){
                                    marker = gmap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(obj.getString("lt")), Double.parseDouble(obj.getString("ln"))))
//                                    .title(extras.getString("vehicleno"))
//                                    .snippet(extras.getString("model"))
                                    .anchor(0.5f,0.5f));
                                }
                                    marker.setPosition(new LatLng(Double.parseDouble(obj.getString("lt")), Double.parseDouble(obj.getString("ln"))));
                                    marker.setRotation(Float.valueOf(obj.getString("dr")));
//
                                    if(obj.getString("im").equals("1")){
                                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon_blue));
                                    }else{
                                        if(obj.getString("ig").equals("0")){
                                            marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon_blue));
                                        }else{
                                            marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon_green));
                                        }
                                    }

                                    Point mappoint = gmap.getProjection().toScreenLocation(new LatLng(Double.parseDouble(obj.getString("lt")), Double.parseDouble(obj.getString("ln"))));
                                    mappoint.set(mappoint.x, mappoint.y+CommonUtils.getPixelValue(35));
                                    LatLng target=gmap.getProjection().fromScreenLocation(mappoint);
//
                                    CameraPosition.Builder builder = new CameraPosition.Builder();
                                    builder.target(target);//target);//
                                    builder.zoom(oldZoomValue);
////                                        builder.tilt(90);
                                    gmap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
//
//                                    oldZoomValue=gmap.getCameraPosition().zoom;
//                                    CustomerApp.showAToast(oldZoomValue+"");
//                                    Point mappoint = gmap.getProjection().toScreenLocation(new LatLng(Double.parseDouble(obj.getString("lt")), Double.parseDouble(obj.getString("ln"))));
//                                    mappoint.set(mappoint.x, mappoint.y+5);
//                                    LatLng target=gmap.getProjection().fromScreenLocation(mappoint);
//
//                                    CameraPosition.Builder builder = new CameraPosition.Builder();
//                                    builder.target(new LatLng(Double.parseDouble(obj.getString("lt")), Double.parseDouble(obj.getString("ln"))));
////                                    builder.zoom(oldZoomValue);
////                                        builder.tilt(90);
//                                    gmap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
                                    gmap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                                        @Override
                                        public void onCameraIdle() {

                                            oldZoomValue=gmap.getCameraPosition().zoom;
                                        }
                                    });

//                                }
                            }
                        }

//                        }else{
//                            CustomerApp.saveToPreferences(TrackActivity.this,ALL_LAT_LNG_ARRAY,allLatLngArray.toString());
//                            new AddToDb().execute();
//                            CustomerApp.consoleLog("NOOOO"+allLatLngArray.length());
//                            allLatLngArray=new JSONArray();
//                            if(CarFinderApp.readFromPreferences(MainActivity.this,"LOADING","").equals("")) {
//                            if(!isGpsRtRunning)
//                                socket.off("gpsrt", handleGPSRT);
//                            }

//                            if(!CarFinderApp.readFromPreferences(TrackActivity.this,ALL_CARS_ARRAY,"").equals("")) {
//                                JSONArray carArray = new JSONArray(CarFinderApp.readFromPreferences(MainActivity.this, ALL_CARS_ARRAY, ""));
//                                new UpdateDb().execute(carArray.toString());
//                            }

//                        }


//                        if(isGpsRtRunning) {
//                            socket.emit("getnextbucket","");
//
//                        }


                    }catch (Exception e){
                        e.printStackTrace();
                    }



                }
            });
        }

    };

    private Emitter.Listener handleDTCRT = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
//                    CustomerApp.consoleLog("DTRT: "+data.toString());
                    if(data.has("mil")) {
                        try {
                            speedometerCustom.updateAllIcons(ENGINE,data.getString("mil"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

    };

    private Emitter.Listener handleCANRT = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject data = (JSONObject) args[0];
//                        CustomerApp.consoleLog("CANRT: "+data.toString());

                        socket.emit("getnextbucket","");

                        if(data.has("rp"))
                            speedometerCustom.setRPM(data.getString("rp").equals("NA") ? 0 :(int) Double.parseDouble(data.getString("rp")));
                        if(data.has("fl"))
                            speedometerCustom.setFuelLevel(data.getString("fl").equals("NA") ? 0 :(int) Double.parseDouble(data.getString("fl")));
                        if(data.has("et"))
                            speedometerCustom.setTemperature(data.getString("et").equals("NA") ? 0 :(int) Double.parseDouble(data.getString("et")));

//                        if(data.getString("od").contains("."))
//                            speedometerCustom.updateodometer(data.getString("od").equals("NA") ? "0" :data.getString("od").substring(0,data.getString("od").indexOf(".")));
//                        else
//                            speedometerCustom.updateodometer(data.getString("od").equals("NA") ? "0" :data.getString("od"));
//                        speedometerCustom.updateSpeed(data.getString("sp").equals("NA") ? "0" :data.getString("sp"));

                        if(data.has("hl"))
                            speedometerCustom.updateAllIcons(HEADLIGHT,data.getString("hl"));
                        if(data.has("ac"))
                            speedometerCustom.updateAllIcons(AC,data.getString("ac"));
                        if(data.has("sb"))
                            speedometerCustom.updateAllIcons(SEATBELT,data.getString("sb"));
                        if(data.has("pb"))
                            speedometerCustom.updateAllIcons(PARKINGBRAKE,data.getString("pb"));


                        //CAN TIME
                        if(data.has("uts")) {
                            speedometerCustom.setCANTime("CAN: "+returnDateFormatted(Long.parseLong(data.getString("uts"))));
                        }

                        speedometerCustom.updateSpeed(data.getString("sp").equals("NA") ? "0" :data.getString("sp"));

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

    };

    public String returnDateFormatted(Long timeinmillis){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeinmillis);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy, HH:mm:ss");
        return  formatter.format(calendar.getTime());
    }


}


