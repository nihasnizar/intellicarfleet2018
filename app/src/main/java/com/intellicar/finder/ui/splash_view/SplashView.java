package com.intellicar.finder.ui.splash_view;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;


import com.intellicar.finder.activities.MainActivity;
import com.intellicar.finder.OnConfirmAlertDialogClickListner;
import com.intellicar.finder.R;
import com.intellicar.finder.libs.svganim.AnimatedSvgView;
import com.intellicar.finder.libs.svganim.SVG;
import com.intellicar.finder.ui.login_view.LoginView;
import com.intellicar.finder.utils.CommonUtils;
import com.intellicar.finder.utils.GlobalPreferManager;
import com.intellicar.finder.utils.anims.Fade;
import com.intellicar.finder.utils.anims.TransitionManager;
import com.intellicar.finder.utils.anims.TransitionSet;
import com.intellicar.finder.utils.extra.Scale;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by snyxius on 3/1/17.
 */


public class SplashView extends Fragment implements ISplashViewControllerCallback,OnConfirmAlertDialogClickListner {

    @BindView(R.id.animated_svg_view)
    AnimatedSvgView svgView;
    @BindView(R.id.test_drive_name)
    LinearLayout testDriveName;
    /*package*/ int index = -1;

    //OTHERS
    private ISplashViewController iSplashViewController;
    OnConfirmAlertDialogClickListner onConfirmAlertDialogClickListner;

    boolean AUTH_FAILURE=false;

    public static SplashView newInstance() {
        Bundle args = new Bundle();
//        position = flag;
        SplashView fragment = new SplashView();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        exitFullscreen(getActivity());
        View view = inflater.inflate(R.layout.splash_view, container, false);
        ButterKnife.bind(this,view);

//
        return view;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GlobalPreferManager.setBoolean("IN_SPLASH_SCREEN",true);

        onConfirmAlertDialogClickListner=this;
        iSplashViewController = new SplashViewController(this);
        if(GlobalPreferManager.getBoolean("IS_LOGIN_DONE",false)) {
            iSplashViewController.checkToken(GlobalPreferManager.getString("TOKEN", ""));
        }

        setSvg(SVG.TESTDRIVE);
        svgView.setViewportSize(505,505);
        svgView.postDelayed(new Runnable() {

            @Override public void run() {
                svgView.start();

                TransitionSet set = new TransitionSet()
                        .addTransition(new Scale(0.7f))
                        .addTransition(new Fade())
                        .setInterpolator(true ? new LinearOutSlowInInterpolator() :
                                new FastOutLinearInInterpolator())
                        .setStartDelay(1000);

                TransitionManager.beginDelayedTransition(testDriveName, set);
                testDriveName.setVisibility(true ? View.VISIBLE : View.INVISIBLE);
            }
        }, 500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!AUTH_FAILURE) {
                    if (GlobalPreferManager.getBoolean("IS_LOGIN_DONE", false)) {
                            startActivity(new Intent(getActivity(), MainActivity.class));
                            GlobalPreferManager.setBoolean("IN_SPLASH_SCREEN", false);
                            getActivity().finish();
                    } else {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, new LoginView(), "LoginView")
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                .commitAllowingStateLoss();
                        GlobalPreferManager.setBoolean("IN_SPLASH_SCREEN", false);
                    }
                }else{
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, new LoginView(), "LoginView")
                            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commitAllowingStateLoss();
                    GlobalPreferManager.setBoolean("IN_SPLASH_SCREEN", false);
                }

            }
        },3000);
    }


    public void setFullscreen(Activity activity) {
        if (Build.VERSION.SDK_INT > 10) {
            int flags = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN;


            activity.getWindow().getDecorView().setSystemUiVisibility(flags);
        } else {
            activity.getWindow()
                    .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }
    public void exitFullscreen(Activity activity) {
        if (Build.VERSION.SDK_INT > 10) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            activity.getWindow()
                    .setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
    }



    private boolean validateField(EditText edt, TextInputLayout txi) {
        if (edt.getText().toString().trim().isEmpty()) {
            txi.setError("Field Required");
            edt.requestFocus();
            return false;
        } else {
            txi.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateField(EditText edt) {
        if (edt.getText().toString().trim().isEmpty()) {
            edt.setError("Field Required");
            edt.requestFocus();
            return false;
        }

        return true;
    }


    private void setSvg(SVG svg) {
        svgView.setGlyphStrings(svg.glyphs);
        svgView.setFillColors(svg.colors);
        svgView.setViewportSize(svg.width, svg.height);
        svgView.setTraceResidueColor(0x32000000);
        svgView.setTraceColors(svg.colors);
        svgView.rebuildGlyphData();
        svgView.start();
    }


    @Override
    public void showProgressIndicator(boolean show) {

    }

    @Override
    public void showServerErrorMessage(String message) {

    }

    @Override
    public void onPositiveButtonClick(String message) {

    }

    @Override
    public void onNegativeButtonClick() {

    }

    @Override
    public void onTokenSuccess(JSONObject responseObj) {

    }

    @Override
    public void onTokenFailed(String message) {
        if(message.contains("Failed to authenticate")){
            GlobalPreferManager.clearSharedPrefData(getActivity());
            CommonUtils.showToast(getActivity(),"Session Expired! Login Again");
            AUTH_FAILURE=false;
        }
    }
}



