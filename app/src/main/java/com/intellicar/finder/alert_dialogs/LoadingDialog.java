package com.intellicar.finder.alert_dialogs;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.intellicar.finder.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by snyxius on 3/1/17.
 */


public class LoadingDialog extends Fragment {


    @BindView(R.id.the_progress_bar)ProgressBar progressBar;
    @BindView(R.id.the_progress_linear)LinearLayout progressLayout;
    @BindView(R.id.loading_text)TextView loadingText;

    public static LoadingDialog newInstance(String loadingText) {
        Bundle args = new Bundle();
//        position = flag;
        args.putString("loadtext",loadingText);
        LoadingDialog fragment = new LoadingDialog();
        fragment.setArguments(args);
        return fragment;
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        Dialog dialog = getDialog();
//        if (dialog != null) {
//            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
////            getDialog().getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
//            getDialog().getWindow().setGravity(Gravity.CENTER);
//        }
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.MATCH_PARENT);
        View view = inflater.inflate(R.layout.loading_view, container, false);
        ButterKnife.bind(this,view);

//
        return view;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadingText.setText("loading");
    }



    public void exitFullscreen(Activity activity) {
        if (Build.VERSION.SDK_INT > 10) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            activity.getWindow()
                    .setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
    }



    private boolean validateField(EditText edt, TextInputLayout txi) {
        if (edt.getText().toString().trim().isEmpty()) {
            txi.setError("Field Required");
            edt.requestFocus();
            return false;
        } else {
            txi.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateField(EditText edt) {
        if (edt.getText().toString().trim().isEmpty()) {
            edt.setError("Field Required");
            edt.requestFocus();
            return false;
        }

        return true;
    }






    }


