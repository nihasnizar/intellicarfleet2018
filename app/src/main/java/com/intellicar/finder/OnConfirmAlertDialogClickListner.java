package com.intellicar.finder;

/**
 * Created by VIDHU on 5/24/2017.
 */

public interface OnConfirmAlertDialogClickListner {

    void onPositiveButtonClick(String message);

    void onNegativeButtonClick();
}
