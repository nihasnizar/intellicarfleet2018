package com.intellicar.finder.libs;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.intellicar.finder.R;
import com.intellicar.finder.libs.roundprogressbar.IconRoundCornerProgressBar;


/**
 * Created by LENOVO on 29-07-2017.
 */

public class Speedometer extends RelativeLayout {

    public static final int DEFAULT_TOTAL_LINES = 85;
    int AC=1,PARKINGBRAKE=2,ENGINE=3,INDICATOR=4,SEATBELT=5,HEADLIGHT=6,MOBILIZE=7,LOCK=8,DOOR_OPEN=9;
    float oldRotation=0;
    float oldZoomValue=17;
    int startvalue=0;

    int oldSpeedValue=0;
    int LargestValue = 7000;
    int totalLines = 85;
    int oldRPMValue = 0, oldFuelValue = 0, oldTempValue = 0;

    TextView firstOdo;
    TextView secondOdo;
    TextView thirdOdo;
    TextView forthOdo;
    TextView fifthOdo;
    TextView sixthOdo;

    ImageView acImage;
    ImageView parkingImage;
    ImageView engineImage;
    ImageView indicatorImage;
    ImageView seatBeltImage;
    ImageView dimImage;
    ImageView immobilizeImage;
    ImageView lockImage;
    ImageView doorOpenImage;

    TextView fuelPercentage;
    TextView tempCelsius;
    IconRoundCornerProgressBar tempProgress;
    IconRoundCornerProgressBar fuelProgress;
//    RelativeLayout speedometerLayout;
//    ImageView speedometerImage;
//    ImageView backArrow;
//    ImageView forwardArrow;
//    LinearLayout speedTextLayout;
//    LinearLayout rpmTextLayout;
    TextView rpmValue,speedValue;
    TextView carBattery,deviceBattery,gpsTime;


    public Speedometer(Context context) {
        super(context);
        init();
    }

    public Speedometer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    private void init() {
//        TypedArray styles = getContext().obtainStyledAttributes(attrs, R.styleable.OtpView);
        LayoutInflater mInflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.dashboard_dialog, this);
//        styleEditTexts(styles);
//        styles.recycle();
         initialiseLayouts();
    }

    private void initialiseLayouts() {
        firstOdo =(TextView)findViewById(R.id.odo_first);
        secondOdo =(TextView)findViewById(R.id.odo_second);
        thirdOdo =(TextView)findViewById(R.id.odo_third);
        forthOdo =(TextView)findViewById(R.id.odo_forth);
        fifthOdo =(TextView)findViewById(R.id.odo_fifth);
        sixthOdo =(TextView)findViewById(R.id.odo_sixth);

        acImage =(ImageView)findViewById(R.id.ac_image);
        parkingImage =(ImageView)findViewById(R.id.parking_image);
        engineImage =(ImageView)findViewById(R.id.engine_image);
        indicatorImage =(ImageView)findViewById(R.id.indicator_image);
        seatBeltImage =(ImageView)findViewById(R.id.seat_belt_image);
        dimImage =(ImageView)findViewById(R.id.dim_image);
        immobilizeImage =(ImageView)findViewById(R.id.immobilize_image);
        lockImage =(ImageView)findViewById(R.id.lock_image);
        doorOpenImage =(ImageView)findViewById(R.id.door_open_image);

        fuelPercentage=(TextView)findViewById(R.id.fuel_percentage);
        tempCelsius=(TextView)findViewById(R.id.temp_celsius);
        fuelProgress=(IconRoundCornerProgressBar) findViewById(R.id.fuel_progress);
        tempProgress=(IconRoundCornerProgressBar) findViewById(R.id.temp_progress);
//        speedometerLayout=(RelativeLayout)findViewById(R.id.speedometer_layout);
//        speedTextLayout=(LinearLayout)findViewById(R.id.speed_text_layout);
//        rpmTextLayout=(LinearLayout)findViewById(R.id.rpm_text_layout);
        speedValue=(TextView)findViewById(R.id.speed_value);
        rpmValue=(TextView)findViewById(R.id.rpm_value);
        carBattery=(TextView)findViewById(R.id.car_battery);
        deviceBattery=(TextView)findViewById(R.id.device_battery);
        gpsTime=(TextView)findViewById(R.id.gps_times);
    }





    private int getPixelValue(int value){
        Resources r = getContext().getApplicationContext().getResources();

        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                value,
                r.getDisplayMetrics()
        );
        return px;

    }


    public void setCarBattery(String value){
        carBattery.setText(value);
    }

    public void setDeviceBattery(String value){
        deviceBattery.setText(value);
    }

    public void setGPSTime(String value){
        gpsTime.setText(value);
    }

    public void setCANTime(String value){
//        gpsTime.setText(value);
    }


    public void updateSpeed(String speed){
        if((int) Double.parseDouble(speed)!=oldSpeedValue) {
            ValueAnimator animator = ValueAnimator.ofInt(oldSpeedValue, (int) Double.parseDouble(speed));
            animator.setDuration(500);
            AccelerateInterpolator decelerateInterpolator = new AccelerateInterpolator(0.8f);
            animator.setInterpolator(decelerateInterpolator);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    char tmp = 0x00B0;
                    speedValue.setText(animation.getAnimatedValue().toString() + "");
                }
            });
            animator.start();
            oldSpeedValue = (int) Double.parseDouble(speed);
        }
    }




    public void setFuelLevel(int percent) {
//        double percentage=((double)percent/(double)100)*fuelMaxValue;
//        SpeedApp.consoleLog("PERCENTAGE FUEL "+percentage+"");
        ValueAnimator animator = ValueAnimator.ofInt(oldFuelValue, (int) percent);
        animator.setDuration(500);
        AccelerateInterpolator decelerateInterpolator = new AccelerateInterpolator(0.8f);
        animator.setInterpolator(decelerateInterpolator);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                fuelProgress.setProgress(Integer.parseInt(animation.getAnimatedValue().toString()));
                fuelPercentage.setText(Integer.parseInt(animation.getAnimatedValue().toString()) + "%");
            }
        });
        animator.start();
        oldFuelValue = percent;
    }

    public void setTemperature(int celsius) {
//        double percentage=(((double)(celsius+tempMinValue)/(double)tempMaxValue)*100);
        ValueAnimator animator = ValueAnimator.ofInt(oldTempValue, (int) celsius);
        animator.setDuration(500);
        AccelerateInterpolator decelerateInterpolator = new AccelerateInterpolator(0.8f);
        animator.setInterpolator(decelerateInterpolator);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                tempProgress.setProgress(Integer.parseInt(animation.getAnimatedValue().toString()));
                char tmp = 0x00B0;
                tempCelsius.setText(Integer.parseInt(animation.getAnimatedValue().toString()) + "" + tmp);
            }
        });
        animator.start();
        oldTempValue = celsius;
    }


    public void setRPM(int value) {
        ValueAnimator animator = ValueAnimator.ofInt(oldRPMValue, value);
        animator.setDuration(500);
        AccelerateInterpolator decelerateInterpolator = new AccelerateInterpolator(0.8f);
        animator.setInterpolator(decelerateInterpolator);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                rpmValue.setText(Integer.parseInt(animation.getAnimatedValue().toString()) + "");
            }
        });
        animator.start();
        oldRPMValue = value;
    }


    public void updateodometer(String odometer) {
        int balance=6-odometer.length();
        if(balance<=0) {
            char first = odometer.charAt(0);
            char second = odometer.charAt(1);
            char third = odometer.charAt(2);
            char forth = odometer.charAt(3);
            char fifth = odometer.charAt(4);
            char sixth = odometer.charAt(5);

            firstOdo.setText(first + "");
            secondOdo.setText(second + "");
            thirdOdo.setText(third + "");
            forthOdo.setText(forth + "");
            fifthOdo.setText(fifth + "");
            sixthOdo.setText(sixth + "");
        }else{
            for(int i=0;i<balance;i++){
                odometer="0"+odometer;
            }
            char first = odometer.charAt(0);
            char second = odometer.charAt(1);
            char third = odometer.charAt(2);
            char forth = odometer.charAt(3);
            char fifth = odometer.charAt(4);
            char sixth = odometer.charAt(5);

            firstOdo.setText(first + "");
            secondOdo.setText(second + "");
            thirdOdo.setText(third + "");
            forthOdo.setText(forth + "");
            fifthOdo.setText(fifth + "");
            sixthOdo.setText(sixth + "");
        }

    }


    public void updateAllIcons(int num,String value) {
//        try {


        if(num==AC) {
            if(value.equals("NA")){
                acImage.setImageResource(R.drawable.ic_ac_grey);
            }else
                acImage.setImageResource(Integer.parseInt(value) >= 1 ? R.drawable.ic_ac_yellow : R.drawable.ic_ac_white);
        }
        if(num==PARKINGBRAKE) {
            if(value.equals("NA")){
                parkingImage.setImageResource(R.drawable.ic_parking_grey);
            }else
                parkingImage.setImageResource(Integer.parseInt(value) >= 1 ? R.drawable.ic_parking_yellow : R.drawable.ic_parking_white);
        }
        if(num==ENGINE) {
            if(value.equals("NA")){
                engineImage.setImageResource(R.drawable.ic_malf_grey);
            }else{
                if (Integer.parseInt(value) > 0) {
                    engineImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_malf_anim));
                    // Get the background, which has been compiled to an AnimationDrawable object.
                    AnimationDrawable frameAnimation = (AnimationDrawable) engineImage.getDrawable();
                    // Start the animation (looped playback by default).
                    frameAnimation.start();
                } else {
                    engineImage.setImageResource(R.drawable.ic_malf_white);
                }

            }
        }
        if(num==INDICATOR) {
            if(value.equals("NA")){
                indicatorImage.setImageResource(R.drawable.ic_indicator_grey);
            }else
                indicatorImage.setImageResource(Integer.parseInt(value) >= 1 ? R.drawable.ic_indicator_yellow : R.drawable.ic_indicator_white);
        }
        if(num==SEATBELT) {
            if(value.equals("NA")){
                seatBeltImage.setImageResource(R.drawable.ic_seatbelt_grey);
            }else{
                if(Integer.parseInt(value) == 1){
                    seatBeltImage.setImageResource(R.drawable.ic_seatbelt_yellow);
                }else{
                    seatBeltImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_seatbelt_anim));

                    // Get the background, which has been compiled to an AnimationDrawable object.
                    AnimationDrawable frameAnimation = (AnimationDrawable) seatBeltImage.getDrawable();
                    // Start the animation (looped playback by default).
                    frameAnimation.start();
//                    seatBeltImage.setImageResource(R.drawable.ic_setbelt_yellow);
                }

            }
        }
        if(num==HEADLIGHT) {
            if(value.equals("NA")){
                dimImage.setImageResource(R.drawable.ic_headlight_grey);
            }else
                dimImage.setImageResource(Integer.parseInt(value) >= 1 ? R.drawable.ic_headlight_yellow : R.drawable.ic_headlight_white);
        }
        if(num==MOBILIZE) {
            if(value.equals("NA")){
                immobilizeImage.setImageResource(R.drawable.ic_immobilize_grey);
            }else{
                if(Integer.parseInt(value) > 0){
                    immobilizeImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_immobilize_anim));
                    // Get the background, which has been compiled to an AnimationDrawable object.
                    AnimationDrawable frameAnimation = (AnimationDrawable) immobilizeImage.getDrawable();
                    // Start the animation (looped playback by default).
                    frameAnimation.start();
                }else{
                    immobilizeImage.setImageResource(R.drawable.ic_immobilize_white);
                }

            }
        }
        if(num==LOCK) {
            if(value.equals("NA")){
                lockImage.setImageResource(R.drawable.ic_doorlock_grey);
            }else
                lockImage.setImageResource(Integer.parseInt(value) >= 1 ? R.drawable.ic_doorlock_yellow : R.drawable.ic_doorlock_white);
        }
        if(num==DOOR_OPEN) {
            if(value.equals("NA")){
                doorOpenImage.setImageResource(R.drawable.ic_door_open);
            }else
                doorOpenImage.setImageResource(Integer.parseInt(value) >= 1 ? R.drawable.ic_door_open_yellow : R.drawable.ic_door_open_white);
        }




//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }




}
