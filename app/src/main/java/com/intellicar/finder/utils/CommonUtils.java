package com.intellicar.finder.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;


import com.intellicar.finder.app.App;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by VIDHU on 12/21/2016.
 */

public class CommonUtils {
    public static final String MOBILE_PATTERN = "[0-9]{10}";
    public static final String PIN_CODE_PATTERN = "[0-9]{6}";
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    //2017-05-30 15:02:20
    private static SimpleDateFormat beforeFormatOne = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    private static SimpleDateFormat beforeFormatOneNEW = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
    private static SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyy-MMM", Locale.US);
    private static SimpleDateFormat appDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    private static SimpleDateFormat monthYearFormat = new SimpleDateFormat("MMM/yyyy", Locale.US);
    private static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private static SimpleDateFormat appTimeFormat = new SimpleDateFormat("hh:mm a", Locale.US);

//    public static void showDatePickerAlert(FragmentActivity activity, Fragment fragment, String selected_date, String tag) {
//
//        DialogFragment newFragment = new DatePickerFragmentDialog();
//        Bundle bundle = new Bundle();
//        bundle.putString("selected_date", selected_date);
//        newFragment.setArguments(bundle);
//        newFragment.setTargetFragment(fragment, 100);
//        newFragment.show(activity.getSupportFragmentManager(), tag);
//
//    }
//
//
//    public static void showAlertDialog(AppCompatActivity activity, String title, String message) {
//
//        AlertDialogFragment newFragment = AlertDialogFragment.newInstance(title, message);
//        newFragment.show(activity.getSupportFragmentManager(), "alert_dialog");
//    }
//
//    public static void showAlertMessageDialog(FragmentActivity activity, String message, String btText) {
//
//        AlertMessageDialogFragment newFragment = AlertMessageDialogFragment.newInstance(message, btText);
//        newFragment.show(activity.getSupportFragmentManager(), "alert_message_dialog");
//    }
//
//
//    public static void showEditDeleteAlertDialog(FragmentActivity activity, Fragment fragment, String projectName, int itemPosition) {
//
//        ProjectEditDeleteDialogFragment newFragment = ProjectEditDeleteDialogFragment.newInstance(projectName, itemPosition);
//        newFragment.setTargetFragment(fragment, 1001);
//        newFragment.show(activity.getSupportFragmentManager(), "edit_delete_alert_dialog");
//    }
//
//    public static void showConfirmAlertFragmentDialog(FragmentActivity activity, Fragment fragment, String title, int message, String buttonYes, String buttonNo, int drawableIcon, OnConfirmAlertDialogClickListner onConfirmAlertDialogClickListner) {
//
//        AlertConfirmFragmentDialog newFragment = AlertConfirmFragmentDialog.newInstance(title ,message, drawableIcon, buttonYes, buttonNo, onConfirmAlertDialogClickListner);
//        newFragment.setTargetFragment(fragment, 1002);
//        newFragment.show(activity.getSupportFragmentManager(), "edit_confirm_alert_dialog");
//    }
//
//    public static void showConfirmAlertFragmentDialogString(FragmentActivity activity, Fragment fragment, String title, String message, String buttonYes, String buttonNo, int drawableIcon, OnConfirmAlertDialogClickListner onConfirmAlertDialogClickListner) {
//
//        AlertConfirmFragmentDialog newFragment = AlertConfirmFragmentDialog.newInstance(title ,message, drawableIcon, buttonYes, buttonNo, onConfirmAlertDialogClickListner);
//        newFragment.setTargetFragment(fragment, 1002);
//        newFragment.show(activity.getSupportFragmentManager(), "edit_confirm_alert_dialog");
//    }
////
//    public static void showConfirmAlertActivityDialogString(FragmentActivity activity, String title, String message, String buttonYes, String buttonNo, int drawableIcon, OnConfirmAlertDialogClickListner onConfirmAlertDialogClickListner) {
//
//        AlertConfirmFragmentDialog newFragment = AlertConfirmFragmentDialog.newInstance(title ,message, drawableIcon, buttonYes, buttonNo, onConfirmAlertDialogClickListner);
//        newFragment.show(activity.getSupportFragmentManager(), "edit_confirm_alert_dialog");
//    }
//
//    public static void showSingleChoiceAlertDialog(FragmentActivity activity, Fragment fragment, String title, String tag, ArrayList<String> listArray) {
//
//        SingleChoiceFragmentDialog newFragment = SingleChoiceFragmentDialog.newInstance(title, tag, listArray);
//        newFragment.setTargetFragment(fragment, 1003);
//        newFragment.show(activity.getSupportFragmentManager(), tag);
//    }
//
//
//    public static void showAddMessageAlertDialog(FragmentActivity activity, Fragment fragment, String title, String hint, String tag) {
//
//        AddMessageFragmentDialog newFragment = AddMessageFragmentDialog.newInstance(title, hint);
//        newFragment.setTargetFragment(fragment, 1004);
//        newFragment.show(activity.getSupportFragmentManager(), tag);
//    }
//
//    public static void showSendFreeMessageAlertDialog(FragmentActivity activity, String projectName, String clientName, String clientNumber, Fragment fragment, String tag) {
//
//        SendSmsDialogFragment newFragment = SendSmsDialogFragment.newInstance(projectName, clientName, clientNumber);
//        newFragment.setTargetFragment(fragment, 1005);
//        newFragment.show(activity.getSupportFragmentManager(), tag);
//    }
//
//    public static void showImageViewDialog(FragmentActivity activity, String url) {
//
//        ImageViewDialogFragment newFragment = ImageViewDialogFragment.newInstance(url);
//        newFragment.show(activity.getSupportFragmentManager(), "image_view_dialog");
//    }
//
//
//    public static void showProgressDialog(FragmentActivity activity, int message) {
//
//        ProgressDialogFragment newFragment = ProgressDialogFragment.newInstance(message);
//        newFragment.show(activity.getSupportFragmentManager(), "progress_dialog");
//    }
//
//    public static void showOtpDialog(AppCompatActivity activity, int message) {
//        OtpDialogFragment newFragment = OtpDialogFragment.newInstance(message);
//        newFragment.show(activity.getSupportFragmentManager(), "otp_dialog_fragment");
//    }
//
//    public static void showTermsDialog(AppCompatActivity activity, String title, String tag) {
//        TermsPrivacyDialogFragment newFragment = TermsPrivacyDialogFragment.newInstance(title, tag);
//        newFragment.show(activity.getSupportFragmentManager(), tag);
//    }

    public static void showSnackbar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public static void showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static int getPixelValue(int value){
        Resources r = App.getAppContext().getResources();

        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                value,
                r.getDisplayMetrics()
        );
        return px;

    }

    public static void consoleLog(String st) {
        Log.e("TESTDRIVE",st);
    }

    //2017-05-30 15:02:20
    //2017-23-05 16:23:54
    public static String getFormattedDate(String date) {
        Date newDate = null;
        DateFormat format=null;
        try {
            format = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
            newDate = beforeFormatOne.parse(date);
        } catch (ParseException e) {
            try {
                newDate=format.parse(date);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
        return appDateFormat.format(newDate);
    }

    public static String getFormattedDateCustom(String date, String custom_format, String output_format) {
        Date newDate = null;
        try {
            DateFormat format = new SimpleDateFormat(custom_format, Locale.ENGLISH);
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat appDateFormatNEW = new SimpleDateFormat(output_format, Locale.US);
        return appDateFormatNEW.format(newDate);
    }

//    public static RequestBody createRequestBody(@NonNull String s) {
//        return RequestBody.create(
//                MediaType.parse("*/*"), s);
//    }


    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        return beforeFormatOne.format(c.getTime());
    }

    public static String getCurrentDisplayDate() {
        Calendar c = Calendar.getInstance();
        return appDateFormat.format(c.getTime());
    }

    public static String getCurrentDisplayTime() {
        Calendar c = Calendar.getInstance();
        return appTimeFormat.format(c.getTime());
    }

    public static String getCurrentMonthYear() {
        Calendar c = Calendar.getInstance();
        return monthYearFormat.format(c.getTime());
    }

    public static String getCurrentMonthYear(String date) {
        Date newDate = null;
        try {
            newDate = yearFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return monthYearFormat.format(newDate);
    }

    public static String getCurrentMonthYearFromFullFormat(String date) {
        Date newDate = null;
        try {
            newDate = beforeFormatOne.parse(date);//01/09/2017 11:00 AM
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return monthYearFormat.format(newDate);
    }

    public static String getYearMonthFormat(String date) {
        Date newDate = null;
        try {
            newDate = yearMonthFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return monthYearFormat.format(newDate);
    }

    public static String getCurrentDate(String date) {
        Date newDate = null;
        try {
            newDate = yearFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return appDateFormat.format(newDate);
    }

    public static boolean isValidMobile(String phone) {
        if (phone.length()!=10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }
    }


    public static String saveToSd(Bitmap bm, String filename) {

        if (bm != null) {
            File ishtaaDir = new File(Environment.getExternalStorageDirectory() + "/Ishtaa/");
            if (!ishtaaDir.exists())
                ishtaaDir.mkdirs();
            File dest = new File(ishtaaDir, filename);
            try {
                FileOutputStream out = new FileOutputStream(dest);
                bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String path = dest.getAbsolutePath();
            Log.d("path ", path);
            return path;
        }

        return null;
    }


    public static Bitmap getBitmapMarker(int ResourceId, int width, int height){
//        int height = 90;
//        int width = 90;
        BitmapDrawable bitmapdraw=(BitmapDrawable)App.getAppContext().getResources().getDrawable(ResourceId);
        Bitmap b=bitmapdraw.getBitmap();
        return Bitmap.createScaledBitmap(b, width, height, false);
    }

}
