package com.intellicar.finder.apis;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.intellicar.finder.utils.GlobalPreferManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by snyxius on 18/1/17.
 */

public class Parse {
    public  static JsonObject loginData(String username, String password){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type","in.intellicar.assets.user.localuser");
        jsonObject.addProperty("username",username);
        jsonObject.addProperty("password",password);

        JsonObject obj=new JsonObject();
        obj.add("user",jsonObject);

        return obj;
    }


    public  static JsonObject speedLimitData(String vp, String speedlimit){
        JsonObject speedlimitjson=new JsonObject();
        speedlimitjson.addProperty("speedlimit",speedlimit);

        JsonObject overspeedalarmjson=new JsonObject();
        overspeedalarmjson.add("overspeedalarm",speedlimitjson);

        JsonObject alarmsjson=new JsonObject();
        alarmsjson.add("alarms",overspeedalarmjson);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("assetpath",vp);
        jsonObject.addProperty("assettype","in.intellicar.assets.vehicle");
        jsonObject.addProperty("hasassetpath",vp);
        jsonObject.addProperty("hasassettype","in.intellicar.assets.vehicle");
        jsonObject.addProperty("bindtag","SELF");
        jsonObject.addProperty("preftag","VEHICLE_INFO");
        jsonObject.add("prefdata",alarmsjson);
        jsonObject.addProperty("token", GlobalPreferManager.getString("TOKEN",""));

        return jsonObject;
    }

    public  static JsonObject trackhistoryData(String vp,String startTime,String endtime,String token){
        JsonObject jsonObject = new JsonObject();
        JsonArray arr=new JsonArray();
        arr.add(vp);
        jsonObject.add("vehiclepath",arr);
        jsonObject.addProperty("vehicletype","in.intellicar.assets.vehicle");
        jsonObject.addProperty("starttime",startTime);
        jsonObject.addProperty("endtime",endtime);
        jsonObject.addProperty("token",token);
        return jsonObject;
    }


    public static JsonObject activateData(String deviceId, String act_code, String token) {
        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("name",name);
        jsonObject.addProperty("deviceid",deviceId);
        jsonObject.addProperty("activation_code" ,act_code);
        jsonObject.addProperty("token",token);
        return jsonObject;
    }

    public  static JsonObject signUpData(String username, String password){

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username",username);
        jsonObject.addProperty("password",password);


        return jsonObject;
    }


    public static JsonObject getonlyTokenData(String token){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("token",token);
        return jsonObject;
    }

    public static JsonObject createVehicleData(String vehiclenum, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("vehicleno",vehiclenum);
        jsonObject.addProperty("token",token);
        return jsonObject;
    }

    public static JsonObject getVehicleData(String metadata, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("pgrouppath",metadata);
        jsonObject.addProperty("token",token);
        return jsonObject;
    }

    public static JSONObject getVehicleDataJSON(String metadata, String token) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("pgrouppath",metadata);
            jsonObject.accumulate("token",token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    public  static JsonObject getAddressData(ArrayList<LatLng> latlng){
        JsonArray latlngarr=new JsonArray();
        for(int i=0;i<latlng.size();i++){
            JsonArray innerlatlngarr=new JsonArray();
            innerlatlngarr.add(String.valueOf(latlng.get(i).latitude));
            innerlatlngarr.add(String.valueOf(latlng.get(i).longitude));
            latlngarr.add(innerlatlngarr);
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.add("data",latlngarr);

        return jsonObject;
    }

    public static JsonObject getVehiclePermission(String vp, String vtype, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("vehiclepath",vp);
        jsonObject.addProperty("vehicletype",vtype);
        jsonObject.addProperty("token",token);
        return jsonObject;
    }

    public static JsonObject getVehicleInfo(String vp, String type, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("assetpath",vp);
        jsonObject.addProperty("assettype",type);
        jsonObject.addProperty("token",token);
        return jsonObject;
    }

    public static JsonObject immobiliseData(String vehiclepath, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("vehiclepath",vehiclepath);
        jsonObject.addProperty("token",token);
        return jsonObject;
    }

}
