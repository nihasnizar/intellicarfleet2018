package com.intellicar.finder.apis;


/**
 * Created by snyxius on 3/1/17.
 */

public class Constants {

    public static final String MESSAGE_SERVER_ERROR = "Something went wrong. Please check with network";
    public static final int DEFAULT_INT = 0;

    public static final String SOCKET_URL="http://assetapi.intellicar.in:10105";
    // public static final String BASE_URL = "http://kafka5.intellicar.in:10104/";
    public static final String BASE_URL = "http://assetapi.intellicar.in:10104/";

    public static final String login="gettoken";
    public static final String getUserInfo="/api/user/getinfo";
    public static final String verifyToken="verifytoken";


    public static final String getmyvehicles="api/vehicle/getmyvehicles";


    //DELETE
//    public static final String getmyvehicles="api/vehicle/getmyvehicles";
//    public static final String trackhistory="api/reports/rtgps/trackhistory";
//
//
//    public static final String trackspeed="api/reports/getdistance";
//    public static final String getalarm="/api/reports/alarm/trackhistory";
//    public static final String getTrip="/api/reports/getvehdailytripreport";
//    public static final String getSingleTripHistory="/api/reports/gettriphistory";
//    public static final String overSpeedCheck="/api/reports/speed/trackhistory";
//    public static final String getVehicleInfo="/api/assetinfo/getallinfoforassetpath";
//    public static final String immobilize="/api/commands/immobilize";
//    public static final String mobilize="/api/commands/mobilize";
//    public static final String ignmobilize="/api/commands/ignmobilize";
//    public static final String ignimmobilize="/api/commands/ignimmobilize";
//
//    public static final String getVechPermision="/api/commands/getvehcmdsperm";
//
//    public static final int SUCCESS_RESULT = 0;
//
//    public static final int FAILURE_RESULT = 1;
//
//    private static final String PACKAGE_NAME =
//            "com.google.android.gms.location.sample.locationaddress";
//
//    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
//
//    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
//
//    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
//    public static final String setspeedlimit="/api/assetinfo/update";
//    public static final String getAddressIntellicar="http://maps.intellicar.in:8082/geocodeint";
}
