package com.intellicar.finder.apis;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by snyxius on 18/1/17.
 */

public interface WebRequests
{


    //@Path annotation on the method parameter, the value of that parameter is bound to the specific replacement block.
    //@Query annotation on a method parameter. They are automatically added at the end of the URL
//    @GET(Constants.corporateContacts+"/{user_id}")
//    Call<JsonObject> getCorporateContacts(@Path("user_id") String user_id);

    @POST(Constants.login)
    Call<JsonObject> login(@Body JsonObject task);

    @POST(Constants.getUserInfo)
    Call<JsonObject> getUserInfo(@Body JsonObject task);

    @POST(Constants.verifyToken)
    Call<JsonObject> verifyToken(@Body JsonObject jsonObject);

    @POST(Constants.getmyvehicles)
    Call<JsonObject> getMyVehicles(@Body JsonObject vehicleDataJSON);


//    //DELETE
//    @POST(Constants.setspeedlimit)
//    Call<JsonObject> setspeedlimit(@Body JsonObject task);
//
//
//    @POST(Constants.getmyvehicles)
//    Call<JsonObject> getVehicles(@Body JsonObject token);
//
//
//    @POST(Constants.trackhistory)
//    Call<JsonObject> trackhistory(@Body JsonObject token);
//
//    @POST(Constants.trackspeed)
//    Call<JsonObject> trackspeed(@Body JsonObject token);
//
//    @POST(Constants.getalarm)
//    Call<JsonObject> getalarm(@Body JsonObject token);
//
//    @POST(Constants.getTrip)
//    Call<JsonObject> trackTrip(@Body JsonObject token);
//
//    @POST(Constants.getSingleTripHistory)
//    Call<JsonObject> getTripHistory(@Body JsonObject token);
//
//    @POST(Constants.getAddressIntellicar)
//    Call<JsonObject> getAddress(@Body JsonObject token);
//
//    @POST(Constants.getVechPermision)
//    Call<JsonObject> getVehiclePermission(@Body JsonObject token);
//
//    @POST(Constants.overSpeedCheck)
//    Call<JsonObject> trackOverSpeed(@Body JsonObject token);
//
//    @POST(Constants.getVehicleInfo)
//    Call<JsonObject> getVehicleInfo(@Body JsonObject token);
//
//
//    @POST(Constants.immobilize)
//    Call<JsonObject> immobilize(@Body JsonObject token);
//
//
//    @POST(Constants.mobilize)
//    Call<JsonObject> mobilize(@Body JsonObject token);
//
//
//    @POST(Constants.ignmobilize)
//    Call<JsonObject> ignmobilize(@Body JsonObject token);
//
//
//    @POST(Constants.ignimmobilize)
//    Call<JsonObject> ignimmobilize(@Body JsonObject token);

}

