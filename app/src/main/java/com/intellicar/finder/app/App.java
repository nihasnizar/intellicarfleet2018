package com.intellicar.finder.app;

import android.app.Application;
import android.content.Context;


import com.intellicar.finder.apis.Constants;
import com.intellicar.finder.utils.GlobalPreferManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nihas on 1/11/2017.
 */

public class App extends Application {

    public static final String BASE_URL = "http://54.201.67.32/rest/";
//    public static final String ISHTAA_BASE_URL = "http://cloudviews.in/ISTHA/api/";
    static OkHttpClient.Builder httpClient = null;
    private static Retrofit retrofitOld = null;
    private static Retrofit retrofit = null;

//    public static DisplayImageOptions options;
    public static Context context;
    public static App mInstance;


    public static Retrofit getClient() {

        if (retrofitOld == null) {
            retrofitOld = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofitOld;
    }

    public static Retrofit getApiClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        mInstance = this;

        GlobalPreferManager.initializePreferenceManager(getApplicationContext());

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);  // <-- this is the important line!

    }



    public static final Retrofit initializeRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }


    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }

    public static Context getInstance() {
        return mInstance;
    }

//    private void initImageLoader(Context context) {
//        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
//        config.threadPriority(Thread.NORM_PRIORITY - 2);
//        config.denyCacheImageMultipleSizesInMemory();
//        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
//        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
//        config.tasksProcessingOrder(QueueProcessingType.LIFO);
//        config.writeDebugLogs();
//        ImageLoader.getInstance().init(config.build());
//
//        options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                .cacheOnDisk(true)//.resetViewBeforeLoading(true)
//                .showImageForEmptyUri(R.drawable.intellicar_logo)
//                .showImageOnFail(R.drawable.intellicar_logo)
//                .showImageOnLoading(R.drawable.intellicar_logo).build();
//
//
//    }



}
