package com.intellicar.finder.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.intellicar.finder.R;
import com.intellicar.finder.apis.Constants;
import com.intellicar.finder.apis.Parse;
import com.intellicar.finder.apis.WebRequest;
import com.intellicar.finder.socket.SocketSingleton;
import com.intellicar.finder.ui.home_view.HomeView;
import com.intellicar.finder.ui.splash_view.SplashView;
import com.intellicar.finder.utils.CommonUtils;
import com.intellicar.finder.utils.GSMInfo;
import com.intellicar.finder.utils.GlobalPreferManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {


    Socket socket;
    JSONArray allLatLngArray=new JSONArray();
    ArrayList<Integer> dataArrayIndex=new ArrayList<>();


    Thread thread;
    Runnable runnable;
    boolean isRunning=false;
    boolean GPSRT_RUN=true;
    boolean isLiveTrack=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new HomeView().newInstance(), "HomeView")
                .commitAllowingStateLoss();

//        new APIcallGetVehicles().execute(Parse.getVehicleDataJSON("/2/6/",GlobalPreferManager.getString("TOKEN","")).toString());


    }

    @Override
    public void onBackPressed() {
        if(isLiveTrack){
            isLiveTrack=false;
            HomeView mainView=(HomeView)getSupportFragmentManager().findFragmentByTag("HomeView");
            if(mainView!=null) {
                mainView.disableLiveTrack();
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    connectSocket();
                }
            },5000);
        }else {
            super.onBackPressed();
        }
    }

    public void startSingleTrack(){
        isLiveTrack=true;
        disconnectSocket();
    }

    @Override
    protected void onResume() {
        super.onResume();
        connectSocket();
        GPSRT_RUN=true;
        dataArrayIndex=new ArrayList<>();
        allLatLngArray=new JSONArray();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disconnectSocket();
    }

    public void connectSocket(){
        socket= SocketSingleton.get(getApplicationContext()).getServerSocket();
        if(!socket.connected()) {
            CommonUtils.consoleLog("CONNECT");
            socket.connect();
            socket.emit("authtoken", GlobalPreferManager.getString( "TOKEN", ""));
            socket.on("authsuccess", handleAuth);
            socket.on("gpsrt", handleGPSRT);
            socket.on("subscribesuccess", handleSubscribeSuccess);
        }
    }

    public void disconnectSocket(){
        if(socket!=null) {
            socket.off("authsuccess", handleAuth);
            socket.off("gpsrt", getSocketId);
            socket.off("subscribesuccess", handleSubscribeSuccess);
//            socket = null;

            socket.disconnect();
            socket.close();
            CommonUtils.consoleLog("DISCONNECT");
        }
    }

    private Emitter.Listener getSocketId = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try{
//                        socketString = args[0].toString();
//                        CarFinderApp.saveToPreferences(getApplicationContext(), "socketId",socketString);
                        Log.d("socketId",args[0].toString());

                        SocketSingleton.get(MainActivity.this).getSocket().emit("authtoken", args[0].toString());

                    } catch (Exception e) {

                    }
                }
            });
        }
    };

    private Emitter.Listener handleAuth = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
//                    CommonUtils.consoleLog("AUTH: "+data.toString());
                    try {
                        JSONObject obj=new JSONObject();

                        JSONArray dataArr=new JSONArray();
                        dataArr.put("gps");//can
                        obj.accumulate("datarequested",dataArr);

                        JSONArray vehiclePathArr=new JSONArray();
                        vehiclePathArr.put("all");// instead all pass vehicle path of selected vehicle
                        obj.accumulate("vehiclepath",vehiclePathArr);

                        JSONArray dataArray=new JSONArray();
                        dataArray.put(obj);

                        JSONObject mainObj=new JSONObject();
                        mainObj.accumulate("data",dataArray);

                        socket.emit("subscribe",mainObj);
//                            socket.emit("getnextbucket","");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });
        }

    };


    private Emitter.Listener handleGPSRT = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONArray data = (JSONArray) args[0];
                        CommonUtils.consoleLog("GPS: "+data);
                        if(GPSRT_RUN) {
                            if (!dataArrayIndex.contains(data.getInt(0))) {
//                                allLatLngArray=new JSONArray();
                                for (int i = 0; i < data.getJSONArray(1).length(); i++) {
                                    JSONObject obj = data.getJSONArray(1).getJSONObject(i);
                                    allLatLngArray.put(obj);
                                }
                                socket.emit("getnextbucket", "");
                                dataArrayIndex.add(data.getInt(0));

                            } else {
                                GlobalPreferManager.setString("ALL_LAT_LNG_ARRAY",allLatLngArray.toString());
                                allLatLngArray = new JSONArray();
                                if(!GlobalPreferManager.getString("ALL_LAT_LNG_ARRAY", "").equals("") &&
                                        !GlobalPreferManager.getString("ALL_CARS", "").equals("")){
                                    CommonUtils.consoleLog("GOT BOTH DATA"+allLatLngArray.length());
                                    GPSRT_RUN=false;

                                    if(!GlobalPreferManager.getBoolean("ADDED_TO_DB", false)){
//                                        GPSRT_RUN=true;
                                        new AddToDb().execute(GlobalPreferManager.getString("ALL_CARS", ""));
                                    }else{
//                                        GPSRT_RUN=true;
                                        JSONArray carArray = new JSONArray(GlobalPreferManager.getString("ALL_CARS_ARRAY", ""));
                                        new UpdateDb().execute(carArray.toString());
                                    }
                                }
//
//                                if (!GlobalPreferManager.getString("ALL_CARS_ARRAY", "").equals("")) {
//                                    JSONArray carArray = new JSONArray(GlobalPreferManager.getString("ALL_CARS_ARRAY", ""));
//                                    new UpdateDb().execute(carArray.toString());
//                                    GPSRT_RUN=false;
//                                }

                            }
                        }


//                        if(isGpsRtRunning) {
//                            socket.emit("getnextbucket","");
//
//                        }


                    }catch (Exception e){
                        e.printStackTrace();
                    }



                }
            });
        }

    };


    private Emitter.Listener handleSubscribeSuccess = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    socket.emit("getnextbucket", "");
                }
            });
        }

    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    /*
    CREATE  A NEW JSON ARRAY MAPPING VEHICLE PATH FROM
    API CALL AND SOCKET AND MERGE BOTH
     */

    public class AddToDb extends AsyncTask<String, Double, JSONArray> {

        @Override
        protected void onProgressUpdate(Double... values) {
                super.onProgressUpdate(values);
                Double actualPercent = values[0] * 100;
                HomeView importFragment = (HomeView) getSupportFragmentManager().findFragmentByTag("HomeView");
                if (importFragment != null)
                    importFragment.showProgressUpdate(actualPercent.intValue());

        }

        @Override
        protected JSONArray doInBackground(String... params) {
            CommonUtils.consoleLog("DO BG ADDTODB");
            JSONArray finalLatLngArray=new JSONArray();
            JSONArray allVehicleArray= null;
            JSONArray latlngArray=null;
            try {
                allVehicleArray = new JSONArray(params[0]);
                latlngArray = new JSONArray(GlobalPreferManager.getString("ALL_LAT_LNG_ARRAY", ""));
            } catch (JSONException e) {
                e.printStackTrace();
                latlngArray=new JSONArray();
                allVehicleArray=new JSONArray();
            }
            try {
//                CarFinderApp.consoleLog(allVehicleArray.length()+"");
                for (int i = 0; i < allVehicleArray.length(); i++) {
                    publishProgress((double)i/(allVehicleArray.length()-1));
//                    if(allVehicleArray.getJSONObject(i).getString("bindtag").equals("VEHICLE_GPS_BIND")) {
                        JSONObject obj = allVehicleArray.getJSONObject(i);
                        for (int j = 0; j < latlngArray.length(); j++) {
                            JSONObject latObj = latlngArray.getJSONObject(j);
                            if(obj.getString("bindtag").equals("VEHICLE_GPS_BIND")) {
                                if (obj.getString("vehiclepath").equals(latObj.getString("vp"))) {
                                    obj.accumulate("lat", latObj.getString("lt"));//latitude
                                    obj.accumulate("lng", latObj.getString("ln"));//longitude
                                    obj.accumulate("ign", latObj.getString("ig"));//ignition
                                    obj.accumulate("dir", latObj.getString("dr"));//direction
                                    obj.accumulate("imm", latObj.has("im") ? latObj.getString("im") : "");//immobilise
                                }
                            }


                        }
                        finalLatLngArray.put(obj);
//                    }

                }

                CommonUtils.consoleLog("DONE ADDTODB");
                GlobalPreferManager.setString("ALL_CARS_ARRAY", finalLatLngArray.toString());





            } catch (JSONException e) {
                e.printStackTrace();
            }

            return finalLatLngArray;
        }

        @Override
        protected void onPostExecute(final JSONArray finalLatLngArray) {
            super.onPostExecute(finalLatLngArray);

            HomeView mainView=(HomeView)getSupportFragmentManager().findFragmentByTag("HomeView");
            if(mainView!=null) {
                mainView.showProgressUpdate(-1);
                mainView.HandleAllMarkerBG();
            }
            GlobalPreferManager.setBoolean("ADDED_TO_DB", true);
            dataArrayIndex=new ArrayList<>();
            GPSRT_RUN=true;
//            if(finalLatLngArray.length()>0) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        GlobalPreferManager.setString("ALL_CARS_ARRAY", finalLatLngArray.toString());
////                        GlobalPreferManager.setString("LOADING", "DONE");
////                        getSupportFragmentManager().beginTransaction()
////                                .replace(R.id.container, new MainView(), "MainView")
////                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
////                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
////                                .commitAllowingStateLoss();
//                    }
//                }, 1000);
//            }else{
//                isRunning=true;
//                CommonUtils.consoleLog("SOCKET NOT CAME");
//                final Handler h=new Handler();
//                runnable=new Runnable() {
//                    public void run(){
//                        while(isRunning){
//                            try{
//                                h.post(new Runnable(){
//                                    public void run(){
//
//                                        if(!GlobalPreferManager.getString("ALL_LAT_LNG_ARRAY", "").equals("")) {
//                                            isRunning=false;
//                                            CommonUtils.consoleLog("GOT FINALLY");
//                                            new AddToDb().execute(GlobalPreferManager.getString("ALL_CARS",""));
//
//                                        }else{
//                                            CommonUtils.consoleLog("STILL NOT CAME RETRYING");
//                                        }
//                                    }
//                                });
//                                TimeUnit.SECONDS.sleep(2);
//                            }
//                            catch(Exception ex){
//                                ex.printStackTrace();
//                            }
//                        }
//                    }
//                };
//                thread=new Thread(runnable);
//                thread.start();
//            }


        }


    }


/*
UPDATE WHEN NEW BEACONS COME
 */


    public class UpdateDb extends AsyncTask<String, Double, JSONArray> {


        @Override
        protected JSONArray doInBackground(String... params) {
            CommonUtils.consoleLog("BG UPDTAE DB");
            JSONArray finalArray=new JSONArray();
            JSONArray allVehicleArray= null;
            JSONArray latlngArray=null;
            try {
                allVehicleArray = new JSONArray(params[0]);
                latlngArray = new JSONArray(GlobalPreferManager.getString("ALL_LAT_LNG_ARRAY", ""));
            } catch (JSONException e) {
                e.printStackTrace();
                latlngArray=new JSONArray();
                allVehicleArray=new JSONArray();
            }
            try {
//                CarFinderApp.consoleLog(allVehicleArray+"");
                for (int i = 0; i < allVehicleArray.length(); i++) {
//                    if(allVehicleArray.getJSONObject(i).getString("bindtag").equals("VEHICLE_GPS_BIND")) {
                        JSONObject obj = allVehicleArray.getJSONObject(i);
                        for (int j = 0; j < latlngArray.length(); j++) {
                            JSONObject latObj = latlngArray.getJSONObject(j);
                            if(obj.getString("bindtag").equals("VEHICLE_GPS_BIND")) {
                                if (obj.getString("vehiclepath").equals(latObj.getString("vp"))) {
                                    obj.remove("lat");
                                    obj.remove("lng");
                                    obj.remove("dir");
                                    obj.remove("imm");
                                    obj.remove("ign");
                                    obj.accumulate("lat", latObj.getString("lt"));
                                    obj.accumulate("lng", latObj.getString("ln"));
                                    obj.accumulate("dir", latObj.getString("dr"));
                                    obj.accumulate("imm", latObj.has("im") ? latObj.getString("im") : "0");
                                    obj.accumulate("ign", latObj.getString("ig"));
                                }
                            }


                        }
                        finalArray.put(obj);
//                    }

                }

                if(finalArray.length()>0) {
                    CommonUtils.consoleLog("UPDATED");
                    GlobalPreferManager.setString("ALL_CARS_ARRAY", finalArray.toString());

                    CommonUtils.consoleLog("CLEARED dataarray");
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            return finalArray;
        }

        @Override
        protected void onPostExecute(final JSONArray contactsArray) {
            super.onPostExecute(contactsArray);

            HomeView mainView=(HomeView)getSupportFragmentManager().findFragmentByTag("HomeView");
            if(mainView!=null)
                mainView.HandleAllMarkerBG();

            dataArrayIndex=new ArrayList<>();
            allLatLngArray=new JSONArray();
            GPSRT_RUN=true;
        }


    }
}
